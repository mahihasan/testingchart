import React from 'react';
import {TouchableOpacity, Text, StyleSheet, Dimensions} from 'react-native';

interface ButtonProps {
  icon: any;
  label: string;
}

const width = (Dimensions.get('window').width - 64) / 2;

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'black',
    padding: 16,
    flexDirection: 'row',
    justifyContent: 'center',
    alignSelf: 'center',
    borderRadius: 16,
    width: width,
  },
  icon: {
    marginRight: 8,
  },
  label: {
    color: 'white',
    fontSize: 16,
    fontWeight: 'bold',
  },
});

const Button = ({label}: ButtonProps) => {
  return (
    <TouchableOpacity style={styles.container}>
      {/* <Icon color="white" name={icon} size={18} style={styles.icon} /> */}
      <Text style={styles.label}>{label}</Text>
    </TouchableOpacity>
  );
};

export default Button;
