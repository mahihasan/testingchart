import React from 'react';
import {StyleSheet, View} from 'react-native';

import Button from './Button';

const styles = StyleSheet.create({
  actions: {
    padding: 16,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
});

const Footer = () => {
  return (
    <View>
      <View style={styles.actions}>
        <Button icon="repeat" label="Swap" />
        <Button icon="send" label="Send" />
      </View>
    </View>
  );
};

export default Footer;
