import React from 'react';
import {GestureHandlerRootView} from 'react-native-gesture-handler';
import Rainbow from './Rainbow';

const App = () => {
  return (
    <GestureHandlerRootView style={{flex: 1}}>
      <Rainbow />
    </GestureHandlerRootView>
  );
};

export default App;
